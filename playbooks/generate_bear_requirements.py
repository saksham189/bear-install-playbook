#!/usr/bin/env python3

import argparse
import collections
import os

from ruamel.yaml import YAML, RoundTripDumper
from ruamel.yaml.comments import CommentedMap

from coalib.collecting.Collectors import get_all_bears

from dependency_management.requirements.AnyOneOfRequirements import (
    AnyOneOfRequirements)

yaml = YAML(typ='rt')
yaml.default_flow_style = False
yaml.Dumper = RoundTripDumper

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
PLAYBOOK_DIR = os.path.abspath(os.path.join(PROJECT_DIR, 'playbooks'))
VARS_DIR = os.path.abspath(os.path.join(PLAYBOOK_DIR, 'vars'))
requirements = CommentedMap()
requirements.yaml_set_start_comment(
    'This is an automatically generated file.\n'
    'And should not be edited by hand.')


def get_args():
    parser = argparse.ArgumentParser(
        description='This program generates a yaml requirement file for '
                    'installation of linters that are used by the bears.')
    parser.add_argument('--bear-dirs', '-d', nargs='+', metavar='DIR',
                        help='additional directories which may contain bears')
    args = parser.parse_args()

    return args


def get_name(req):
    req = req.lower().partition('requirement')
    return req[0] + '_' + req[1]


def helper(d, reqs):
    for req in reqs:
        if(isinstance(req, AnyOneOfRequirements)):
            helper(d, req.requirements)
        else:
            entry = {}
            entry['version'] = req.version
            entry['name'] = req.package
            d[get_name(type(req).__name__)].append(dict([('package', entry)]))


if __name__ == '__main__':
    args = get_args()
    if args.bear_dirs is not None:
        bear_dirs.extend(args.bear_dirs)

    for bear in get_all_bears():
        d = collections.defaultdict(list)
        helper(d, bear.REQUIREMENTS)
        d = dict(d)
        fille_path = os.path.join(VARS_DIR, bear.name.lower() + '.yml')

        with open(file_path, 'w') as f:
            yaml.dump(d, f)
