FROM ansible/ansible:ubuntu1604

# Set the locale
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    PATH=$PATH:/root/pmd-bin-5.4.1/bin:/root/dart-sdk/bin:/coala-bears/node_modules/.bin:/root/bakalint-0.4.0:/root/elm-format-0.18 \
    NODE_PATH=/coala-bears/node_modules

# Create symlink for cache
RUN mkdir -p /root/.local/share/coala && \
  ln -s /root/.local/share/coala /cache


# pip3 setup
RUN apt-get update && apt-get install -y python3-pip
RUN pip3 install --upgrade pip==9.0.3
RUN pip3 --version
RUN python3 --version

RUN pip3 install ansible
RUN ansible --version

# ansible setup
ADD playbooks/ /etc/ansible/
ADD tests/bear_names.yml /etc/ansible
ADD tests/install_manager.yml /etc/ansible
WORKDIR /etc/ansible
RUN ansible-galaxy install geerlingguy.nodejs oefenweb.r fubarhouse.golang
RUN ansible-playbook install_manager.yml
RUN ansible-playbook main.yml -e "@bear_names.yml"

# Coala setup
RUN pip3 install --upgrade setuptools
RUN cd / && \
  git clone --depth 1 https://github.com/coala/coala-bears.git && \
  git clone --depth 1 https://github.com/coala/coala.git && \
  pip3 install --no-cache-dir -U \
    -e /coala \
    -r /coala-bears/test-requirements.txt
